package slack

/*
Usage:

  s := NewSlackMessage()
  s.WebhookURL = webhookURL
  s.SetFieldMessage("test _field_ message")
  s.SetMessage("test *message*")

  err := s.SendMessage()
*/

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
)

// SlackColorSuccess is green.
// SlackColorWarning is yellow.
// SlackColorFail is red.
const (
	SlackColorSuccess = "good"
	SlackColorWarning = "warning"
	SlackColorFail    = "danger"
)

// SlackField is the struct for Slack attachments fields.
type SlackField struct {
	Title string `json:"title"`
	Value string `json:"value"`
	Short bool   `json:"short"`
}

// SlackAttachment is the struct for Slack attachments
type SlackAttachment struct {
	Fallback   string        `json:"fallback,omitempty"`
	Color      string        `json:"color,omitempty"`
	Pretext    string        `json:"pretext,omitempty"`
	Title      string        `json:"title,omitempty"`
	TitleLink  string        `json:"title_link,omitempty"`
	Text       string        `json:"text,omitempty"`
	Fields     []*SlackField `json:"fields,omitempty"`
	ImageURL   string        `json:"image_url,omitempty"`
	ThumbURL   string        `json:"thumb_url,omitempty"`
	Footer     string        `json:"footer,omitempty"`
	FooterIcon string        `json:"footer_icon,omitempty"`
	MrkdwnIn   [3]string     `json:"mrkdwn_in,omitempty"`
	Ts         int           `json:"ts,omitempty"`
}

// SlackMessage is struct for the message of slack.
type SlackMessage struct {
	Text      string `json:"text,omitempty"`
	BotName   string `json:"bot_name,omitempty"`
	IconEmoji string `json:"icon_emoji,omitempty"`
	IconURL   string `json:"icon_url,omitempty"`
	// Channel     string              `json:"-"`
	WebhookURL  string              `json:"-"`
	Attachments [1]*SlackAttachment `json:"attachments,omitempty"`
	Mrkdwn      string              `json:"mrkdwn,omitempty"`
}

// SlackOption allows SlackMessage to be optionally configured.
type SlackOption func(s *SlackMessage)

// NewSlackMessage is the constructor of SlackMessage struct.
func NewSlackMessage(message, webHookURL string, options ...SlackOption) (*SlackMessage, error) {

	if webHookURL == "" {
		return &SlackMessage{}, fmt.Errorf("%v", "No web hook URL.")
	}

	if message == "" {
		return &SlackMessage{}, fmt.Errorf("%v", "No message.")
	}

	a := [1]*SlackAttachment{&SlackAttachment{MrkdwnIn: [3]string{"text", "pretext", "fields"}}}
	a[0].Fields = []*SlackField{&SlackField{}}
	// a[0].Pretext = message
	a[0].Text = message
	// a[0].Fields[0].Value = message
	// a[0].Fallback = message

	s := &SlackMessage{
		WebhookURL:  webHookURL,
		Attachments: a,
	}

	for _, option := range options {
		option(s)
	}

	return s, nil
}

// WithFieldMessage set the field message.
func WithFieldMessage(text string) SlackOption {
	return func(s *SlackMessage) {
		s.Attachments[0].Fields[0].Value = text
		s.Attachments[0].Fallback = text
	}
}

// WithFieldTitle set the title of the message.
func WithFieldTitle(text string) SlackOption {
	return func(s *SlackMessage) {
		s.Attachments[0].Title = text
		// s.Attachments[0].Fields[0].Title = text
	}
}

// WithFieldTitleURL set the url of the title.
func WithFieldTitleURL(url string) SlackOption {
	return func(s *SlackMessage) {
		s.Attachments[0].TitleLink = url
	}
}

// WithColor set the color.
func WithColor(color string) SlackOption {
	return func(s *SlackMessage) {
		s.Attachments[0].Color = color
	}
}

// WithIconURL set the url of icon.
func WithIconURL(url string) SlackOption {
	return func(s *SlackMessage) {
		s.IconURL = url
	}
}

// SendMessage send the message to slack.
func (s *SlackMessage) SendMessage() error {

	params, jerr := json.Marshal(s)
	if jerr != nil {
		fmt.Printf("*** fail to send message: %v", jerr)
	}

	// res, rerr := http.PostForm(
	// 	s.WebhookURL,
	// 	url.Values{"payload": {string(params)}},
	// )
	// if rerr != nil {
	// 	fmt.Printf("*** fail to post: %v", rerr)
	// 	return rerr
	// }

	// fmt.Println("webhook: " + s.WebhookURL)
	// fmt.Printf("slack json: %v", string(params))

	values := url.Values{}
	values.Set("payload", string(params))

	req, rerr := http.NewRequest(
		"POST",
		s.WebhookURL,
		bytes.NewBuffer(params),
	)

	if rerr != nil {
		fmt.Printf("*** fail to create request: %v", rerr)
		return rerr
	}

	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	res, err := client.Do(req)
	if err != nil {
		fmt.Printf("*** fail to post: %v", err)
	}
	defer func() error {
		err := res.Body.Close()
		if err != nil {
			fmt.Printf("*** fail to close body: %v", err)
			return err
		}

		return nil
	}()

	_, berr := ioutil.ReadAll(res.Body)
	if berr != nil {
		fmt.Printf("*** fail to read body: %v", berr)

		return berr
	}
	// fmt.Printf("slack body: %v", string(body))

	return nil
}

// SetMessage set the message for slack.
func (s *SlackMessage) SetMessage(text string) {
	// s.Text = text
	// s.Attachments[0].Pretext = text
	s.Attachments[0].Text = text

	return
}

// SetFieldTitle set the attachment title message for slack.
func (s *SlackMessage) SetFieldTitle(title, url string) {
	s.Attachments[0].Title = title
	// s.Attachments[0].Fields[0].Title = title

	return
}

func SendSlackMessage(mes, webhookURL, title, color, iconURL string) error {
	ePrefix := "*** SendSlackMessage:"

	if mes == "" {
		return fmt.Errorf("%v %v", ePrefix, "No message.")
	}

	if title == "" {
		return fmt.Errorf("%v %v", ePrefix, "No title.")
	}

	if webhookURL == "" {
		return fmt.Errorf("%v %v", ePrefix, "No webhook URL.")
	}

	s, err := NewSlackMessage(
		mes,
		webhookURL,
		WithFieldTitle(title),
		WithColor(color),
		WithIconURL(iconURL),
	)
	if err != nil {
		return err
	}

	s.SendMessage()

	return nil
}

// SendSlackSuccessMessage send the message to slack with green.
func SendSlackSuccessMessage(mes, webhookURL, title, iconURL string) error {
	return SendSlackMessage(mes, webhookURL, title, SlackColorSuccess, iconURL)
}

// SendSlackFailMessage send the message to slack with red.
func SendSlackFailMessage(mes, webhookURL, title, iconURL string) error {
	return SendSlackMessage(mes, webhookURL, title, SlackColorFail, iconURL)
}
