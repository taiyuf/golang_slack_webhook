golang_slack_webhook
====

# 使い方

```
  msg := hoge
  uri := "https://hook.slack.com/..."
  s, err = slack.NewSlackMessage(
    msg,
    uri,
    WithFieldTitle(title),
    WithColor(color),
  )
  if err != nil {
    return err
  }

  err := s.SendMessage()

  # or

  err := SendSLackMessage(message, webHookURL, title, color, iconURL)
  err := SendSlackSuccessMessage(message, webHookURL, title, iconURL)
  err := SendSlackFailMessage(message, webHookURL, title, iconURL)
```
